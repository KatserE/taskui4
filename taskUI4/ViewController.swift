//
//  ViewController.swift
//  taskUI4
//
//  Created by Евгений Кацер on 19.05.2023.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UserSettings.color.ToUIColor()
    }

    @IBAction func changeColorButtonPressed(_ sender: Any) {
        UserSettings.color = Color.random()
        print(UserSettings.color)
        self.view.backgroundColor = UserSettings.color.ToUIColor()
    }
}

