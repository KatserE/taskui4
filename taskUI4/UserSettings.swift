//
//  UserSettings.swift
//  taskUI4
//
//  Created by Евгений Кацер on 19.05.2023.
//

import UIKit

struct UserSettings {
    private static let colorID = "color"
    static var color: Color {
        get {
            guard let saved = UserDefaults.standard.data(
                        forKey: UserSettings.colorID
                  ),
                  let decoded = try? NSKeyedUnarchiver.unarchivedObject(
                        ofClass: Color.self,
                        from: saved
                  ) else {
                return Color.white
            }
            
            return decoded
        }
        set {
            let archived = try? NSKeyedArchiver.archivedData(
                withRootObject: newValue, requiringSecureCoding: false
            )
            
            if let archived {
                UserDefaults.standard.set(archived, forKey: UserSettings.colorID)
            }
        }
    }
}
