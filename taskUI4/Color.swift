//
//  Color.swift
//  taskUI4
//
//  Created by Евгений Кацер on 19.05.2023.
//

import UIKit

class Color : NSObject, NSCoding, NSSecureCoding {
    private static let redID = "red"
    private static let greenID = "green"
    private static let blueID = "blue"
    private static let alphaID = "alpha"
    
    static let supportsSecureCoding = true

    let red: Float
    let green: Float
    let blue: Float
    let alpha: Float    
    
    required init?(coder: NSCoder) {
        red = coder.decodeFloat(forKey: Color.redID)
        green = coder.decodeFloat(forKey: Color.greenID)
        blue = coder.decodeFloat(forKey: Color.blueID)
        alpha = coder.decodeFloat(forKey: Color.alphaID)
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(red, forKey: Color.redID)
        coder.encode(green, forKey: Color.greenID)
        coder.encode(blue, forKey: Color.blueID)
        coder.encode(alpha, forKey: Color.alphaID)
    }
    
    init(red: Float, green: Float, blue: Float, alpha: Float) {
        self.red = red
        self.green = green
        self.blue = blue
        self.alpha = alpha
    }
    
    func ToUIColor() -> UIColor {
        UIColor(red: CGFloat(red), green: CGFloat(green),
                blue: CGFloat(blue), alpha: CGFloat(alpha))
    }
    
    static let white = Color(red: 1, green: 1, blue: 1, alpha: 1)
    
    static func random() -> Color {
        let colorRange: ClosedRange<Float> = 0.0...1.0
        
        let red = Float.random(in: colorRange)
        let green = Float.random(in: colorRange)
        let blue = Float.random(in: colorRange)
        
        return Color(red: red, green: green, blue: blue, alpha: 1)
    }
}
